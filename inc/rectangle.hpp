#pragma once

class Rectangle_for_game
{
private:
    int length;
    int width;
    int coordinate_x;
    int coordinate_y;

public:
    int GetLength();

    int GetWidth();

    int Get_coordinate_x();

    int Get_coordinate_y();

    void Set_Length_coor_x(int Value_Length, int Value_coor_x);

    void Set_width(int Value_width);

    void Set_coordinate_y(int Value);

    void Set_coordinate_x(int Value);

    void Up_coordinate_y(int Value);

    void Down_coordinate_y(int Value);

    ~Rectangle_for_game();
};