#pragma once

class Circle_for_game
{
private:
    int Radius;
    int coordinate_x;
    int coordinate_y;

public:
    int GetRadius();

    int Get_coordinate_x();

    int Get_coordinate_y();

    void Set_Length_coor(int Value_Radius, int Value_coor_x, int Valur_coor_y);

    void Set_coordinate_y(int Value);

    void Set_coordinate_x(int Value);

    void work_with_coordinate(int Value_x, int Value_y);

    ~Circle_for_game();
};