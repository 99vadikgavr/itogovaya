﻿#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <ctime>
#include <chrono>
#include <thread>
#include <sstream>
#include <circle.hpp>
#include <rectangle.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;

std::string IntToString(int a)
{
    std::ostringstream temp;
    temp << a;
    return temp.str();
}

int main()
{
    srand(time(0));

    bool flag_start = 1;
    bool flag_lose = 0;
    int score_Player = 0;
    int score_Computer = 0;
    std::string str_score_Computer = "0";
    std::string str_score_Player = "0";
    int random = 0;
    int queue = 0;
    const int length_window = 1600;
    const int width_window = 900;

    Rectangle_for_game Player;
    Rectangle_for_game Computer;
    Circle_for_game Circle;

    Player.Set_Length_coor_x(20, 30);
    Player.Set_coordinate_y(450);
    Player.Set_width(100);

    Computer.Set_Length_coor_x(20, length_window - 30);
    Computer.Set_coordinate_y(450);
    Computer.Set_width(100);

    Circle.Set_Length_coor(15, length_window / 2, width_window / 2);

    sf::Font font;
    if (!font.loadFromFile("font/arial.ttf"))
        return -8;
    sf::Text text_player;
    text_player.setFont(font);
    text_player.setCharacterSize(36);
    text_player.setPosition(600, 0);
    text_player.setFillColor(sf::Color::White);

    sf::Text text_computer;
    text_computer.setFont(font);
    text_computer.setCharacterSize(36);
    text_computer.setPosition(850, 0);
    text_computer.setFillColor(sf::Color::White);

    sf::Text text_start;
    text_start.setFont(font);
    text_start.setCharacterSize(80);
    text_start.setPosition(375, 350);
    text_start.setFillColor(sf::Color::White);

    sf::SoundBuffer buffer_racket;
    if (!buffer_racket.loadFromFile("sounds/racket.WAV"))
        return -7;
    sf::Sound sound_racket;
    sound_racket.setBuffer(buffer_racket);

    sf::SoundBuffer buffer_wall_1;
    if (!buffer_wall_1.loadFromFile("sounds/Sound_wall_1.wav"))
        return -6;
    sf::Sound sound_wall_1;
    sound_wall_1.setBuffer(buffer_wall_1);

    sf::SoundBuffer buffer_wall_2;
    if (!buffer_wall_2.loadFromFile("sounds/Sound_wall_2.wav"))
        return -5;
    sf::Sound sound_wall_2;
    sound_wall_2.setBuffer(buffer_wall_2);

    sf::SoundBuffer buffer_Lose;
    if (!buffer_Lose.loadFromFile("sounds/Sound_Lose.wav"))
        return -4;
    sf::Sound sound_Lose;
    sound_Lose.setBuffer(buffer_Lose);

    sf::SoundBuffer buffer_Wint;
    if (!buffer_Wint.loadFromFile("sounds/Sound_Win.wav"))
        return -3;
    sf::Sound sound_Win;
    sound_Win.setBuffer(buffer_Wint);

    sf::RectangleShape border_up(sf::Vector2f(length_window, 6));
    border_up.setPosition(0, 47);
    border_up.setOrigin(0, 3);
    border_up.setFillColor(sf::Color::White);

    sf::RectangleShape border_down(sf::Vector2f(length_window, 6));
    border_down.setPosition(0, width_window - 6);
    border_down.setFillColor(sf::Color::White);

    sf::RectangleShape border_black_background(sf::Vector2f(length_window, 47));
    border_black_background.setPosition(0, 0);
    border_black_background.setFillColor(sf::Color::Black);

    sf::Texture texture;
    if (!texture.loadFromFile("img/tennis_table.jpg"))
        return -2;
    sf::Sprite sprite(texture);

    sf::RenderWindow window(sf::VideoMode(length_window, width_window), L"Пинг-понг!");

    sf::Image icon;
    if (!icon.loadFromFile("img/ikonka.png"))
        return -1;
    window.setIcon(32, 32, icon.getPixelsPtr());

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        //Начальное окно
        if (flag_start)
        {
            text_start.setString("Welcome to ping pong !");
            window.draw(text_start);
            window.display();
            std::this_thread::sleep_for(2s);
            text_start.setCharacterSize(80);
            text_start.setPosition(200, 350);
            text_start.setString("Use the up key or the down key !");
            window.draw(text_start);
            window.display();
            std::this_thread::sleep_for(5s);
        }


        //Обновление голов
        text_player.setString("Player: " + str_score_Player);
        text_computer.setString("Computer: " + str_score_Computer);

        //создание Объектов по актуальным координатам
        sf::CircleShape circle(Circle.GetRadius());
        circle.setPosition(Circle.Get_coordinate_x(), Circle.Get_coordinate_y());
        circle.setOrigin(Circle.GetRadius(), Circle.GetRadius());
        circle.setFillColor(sf::Color::White);

        sf::RectangleShape Player_Rectangle(sf::Vector2f(Player.GetLength(), Player.GetWidth()));
        Player_Rectangle.setPosition(Player.Get_coordinate_x(), Player.Get_coordinate_y());
        Player_Rectangle.setOrigin((Player.GetLength() / 2), (Player.GetWidth() / 2));
        Player_Rectangle.setFillColor(sf::Color::White);

        sf::RectangleShape Computer_Rectangle(sf::Vector2f(Computer.GetLength(), Computer.GetWidth()));
        Computer_Rectangle.setPosition(Computer.Get_coordinate_x(), Computer.Get_coordinate_y());
        Computer_Rectangle.setOrigin((Computer.GetLength() / 2), (Computer.GetWidth() / 2));
        Computer_Rectangle.setFillColor(sf::Color::White);

        //Проверка на пропуск гола и заморозка игры на 2 сек.
        if (flag_lose)
        {
            window.clear();
            window.draw(sprite);
            window.draw(border_black_background);
            window.draw(text_player);
            window.draw(text_computer);
            window.draw(border_up);
            window.draw(border_down);
            window.draw(Player_Rectangle);
            window.draw(Computer_Rectangle);
            window.draw(circle);
            window.display();
            std::this_thread::sleep_for(2s);
            flag_lose = 0;
        }

        //с клавиатуры управляем ракеткой двумя клавишами
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            if (Player.Get_coordinate_y() - Player.GetWidth() / 2 >= 47)
                Player.Up_coordinate_y(4);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            if (Player.Get_coordinate_y() + Player.GetWidth() / 2 <= 897)
                Player.Down_coordinate_y(4);
        }

        //Встреча шарика с игроком
        if (Circle.Get_coordinate_x() - Circle.GetRadius() <= Player.Get_coordinate_x() + 10 &&
            Circle.Get_coordinate_y() <= Player.Get_coordinate_y() + 65 &&
            Circle.Get_coordinate_y() >= Player.Get_coordinate_y() - 65)
        {
            sound_racket.play();
            if (queue == 0)
                random = 1 + rand() % 6;
            queue = 1;
        }

        //Встреча шарика с компьютером
        if (Circle.Get_coordinate_x() + Circle.GetRadius() >= Computer.Get_coordinate_x() - 10 &&
            Circle.Get_coordinate_y() <= Computer.Get_coordinate_y() + 65 &&
            Circle.Get_coordinate_y() >= Computer.Get_coordinate_y() - 65)
        {
            sound_racket.play();
            queue = 2;
        }

        //Встреча шарика с горизонтальными стенами
        if (Circle.Get_coordinate_y() - Circle.GetRadius() <= 47)
        {
            random = 1 + rand() % 3;
            if (random == 1 || random == 2)
                sound_wall_1.play();
            else if (random == 3)
                sound_wall_2.play();
        }
        else if (Circle.Get_coordinate_y() + Circle.GetRadius() >= 897)
        {
            random = 4 + rand() % 6;
            if (random == 4 || random == 5)
                sound_wall_2.play();
            else if (random == 6)
                sound_wall_1.play();
        }
        //Отбивание шарика компьютером
        if (queue == 1)
        {
            if (Computer.Get_coordinate_y() < Circle.Get_coordinate_y() && Computer.Get_coordinate_y() <= 897 - Computer.GetWidth() / 2)
                Computer.Down_coordinate_y(3);
            else if (Computer.Get_coordinate_y() > Circle.Get_coordinate_y() && Computer.Get_coordinate_y() >= 47 + Computer.GetWidth() / 2)
                Computer.Up_coordinate_y(3);
        }

        //Возвращение комьютера в центр
        else
        {
            if (Computer.Get_coordinate_y() < width_window / 2)
                Computer.Down_coordinate_y(2);
            else if (Computer.Get_coordinate_y() > width_window / 2)
                Computer.Up_coordinate_y(2);
        }

        //Обработка очереди, и движение шарика
        if (queue == 0)
        {
            if (flag_start)
            {
                window.clear();
                window.draw(sprite);
                window.draw(border_black_background);
                window.draw(text_player);
                window.draw(text_computer);
                window.draw(border_up);
                window.draw(border_down);
                window.draw(Player_Rectangle);
                window.draw(Computer_Rectangle);
                window.draw(circle);
                window.display();
                std::this_thread::sleep_for(2s);
                flag_start = 0;
            }
            Circle.work_with_coordinate(-7, 0);
        }
        else if (queue == 1)
        {
            if (random == 1)
                Circle.work_with_coordinate(7, 2);
            if (random == 2)
                Circle.work_with_coordinate(7, 3);
            if (random == 3)
                Circle.work_with_coordinate(7, 4);
            if (random == 4)
                Circle.work_with_coordinate(7, -2);
            if (random == 5)
                Circle.work_with_coordinate(7, -3);
            if (random == 6)
                Circle.work_with_coordinate(7, -4);
        }
        else if (queue == 2)
        {
            if (random == 1)
                Circle.work_with_coordinate(-7, 2);
            if (random == 2)
                Circle.work_with_coordinate(-7, 3);
            if (random == 3)
                Circle.work_with_coordinate(-7, 4);
            if (random == 4)
                Circle.work_with_coordinate(-7, -2);
            if (random == 5)
                Circle.work_with_coordinate(-7, -3);
            if (random == 6)
                Circle.work_with_coordinate(-7, -4);
        }

        //Пропуск шарика одной из сторон
        if (Circle.Get_coordinate_x() - Circle.GetRadius() < 10)
        {
            score_Computer++;
            str_score_Computer = "";
            str_score_Computer += IntToString(score_Computer);
            Player.Set_coordinate_y(width_window / 2);
            Computer.Set_coordinate_y(width_window / 2);
            Circle.Set_coordinate_x(length_window / 2), Circle.Set_coordinate_y(width_window / 2);
            queue = 0;
            flag_lose = 1;
        }
        else if (Circle.Get_coordinate_x() + Circle.GetRadius() > length_window - 10)
        {
            score_Player++;
            str_score_Player = "";
            str_score_Player += IntToString(score_Player);
            Player.Set_coordinate_y(width_window / 2);
            Computer.Set_coordinate_y(width_window / 2);
            Circle.Set_coordinate_x(length_window / 2), Circle.Set_coordinate_y(width_window / 2);
            queue = 0;
            flag_lose = 1;
        }

        window.clear();
        //Проверка на победу
        if ((score_Player >= 11 && score_Computer < score_Player - 1)  || (score_Player == 7 && score_Computer == 0))
        {
            sound_Win.play();
            text_player.setString("Player Win !");
            text_player.setPosition(550, 350);
            text_player.setCharacterSize(80);
            window.draw(text_player);
            window.display();
            std::this_thread::sleep_for(5s);
            window.close();
            return 1;

        }
        //Проверка на поражение
        else if ((score_Computer >= 11 && score_Player < score_Computer - 1) || (score_Computer == 7 && score_Player == 0))
        {
            sound_Lose.play();
            text_computer.setString("Computer Win !");
            text_computer.setPosition(500, 350);
            text_computer.setCharacterSize(80);
            window.draw(text_computer);
            window.display();
            std::this_thread::sleep_for(5s);
            window.close();
            return 2;
        }
        window.draw(sprite);
        window.draw(border_black_background);
        window.draw(text_player);
        window.draw(text_computer);
        window.draw(border_up);
        window.draw(border_down);
        window.draw(Player_Rectangle);
        window.draw(Computer_Rectangle);
        window.draw(circle);
        window.display();
    }
    return 0;
}
