#include <rectangle.hpp>

int Rectangle_for_game::GetLength()
{
    return length;
}

int Rectangle_for_game::GetWidth()
{
    return width;
}

int Rectangle_for_game::Get_coordinate_x()
{
    return coordinate_x;
}

int Rectangle_for_game::Get_coordinate_y()
{
    return coordinate_y;
}

void Rectangle_for_game::Set_Length_coor_x(int Value_Length, int Value_coor_x)
{
    length = Value_Length;
    coordinate_x = Value_coor_x;
}

void Rectangle_for_game::Set_width(int Value_width)
{
    width = Value_width;
}

void Rectangle_for_game::Set_coordinate_y(int Value)
{
    coordinate_y = Value;
}

void Rectangle_for_game::Set_coordinate_x(int Value)
{
    coordinate_x = Value;
}

void Rectangle_for_game::Up_coordinate_y(int Value)
{
    coordinate_y = coordinate_y - Value;
}

void Rectangle_for_game::Down_coordinate_y(int Value)
{
    coordinate_y = coordinate_y + Value;
}

Rectangle_for_game::~Rectangle_for_game()
{

}