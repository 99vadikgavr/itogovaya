;  
#define   Name       "Ping_pong"
;  
#define   Version    "1.0"
;
#define   Publisher   "Vadim Gavrilov"
;   
#define   URL        "http://www.vk.com/id358778371"
;   
#define   ExeName    "Ping_pong.exe"

;------------------------------------------------------------------------------
;    
;------------------------------------------------------------------------------
[Setup]

;
AppId={{6CBDEA1C-F6E5-4F57-93F9-DF6CAA7364FC}
;  
AppName={#Name}
AppVersion={#Version}
AppPublisher={#Publisher}
AppPublisherURL={#URL}
AppSupportURL={#URL}
AppUpdatesURL={#URL}

; 
DefaultDirName={pf}\{#Name}
; 
DefaultGroupName={#Name}

;    
OutputDir=C:\Git\itogovaya\Installer\output
OutputBaseFileName=Ping_pong

;  
SetupIconFile=C:\Git\itogovaya\img\ikonka.ico

;  
Compression=lzma
SolidCompression=yes

;------------------------------------------------------------------------------
;       
;------------------------------------------------------------------------------
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile: "License_ENG.txt"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "License_RUS.txt"

;------------------------------------------------------------------------------
;    
;------------------------------------------------------------------------------
[Tasks]
;     
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

;------------------------------------------------------------------------------
;    
;------------------------------------------------------------------------------
[Files]

;  
Source: "C:\build\itogovaya\Release\Ping_pong.exe"; DestDir: "{app}"; Flags: ignoreversion

;  
Source: "C:\Git\itogovaya\font\*"; DestDir: "{app}\font\"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Git\itogovaya\img\*"; DestDir: "{app}\img\"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Git\itogovaya\sounds\*"; DestDir: "{app}\sounds\"; Flags: ignoreversion recursesubdirs createallsubdirs

; VS Redistributable package
Source: "C:\Git\itogovaya\Installer\VC_redist.x64.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall
;------------------------------------------------------------------------------
;    
;------------------------------------------------------------------------------ 
[Icons]

Name: "{group}\{#Name}"; Filename: "{app}\{#ExeName}"

Name: "{commondesktop}\{#Name}"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon

[Run]
;------------------------------------------------------------------------------
;
;------------------------------------------------------------------------------
Filename: {tmp}\VC_redist.x64.exe; Parameters: "/q:a /c:""install /l /q"""; StatusMsg: VS redistributable package is installed. Please wait...
